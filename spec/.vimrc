set t_Co=16

set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle.vim
call vundle#begin()

Bundle 'gmarik/vundle.vim'

Plugin 'ToadJamb/vim_alternate_file'

call vundle#end()
filetype plugin indent on
